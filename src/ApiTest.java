import thsst.ontopop.api.Condition;
import thsst.ontopop.api.Food;
import thsst.ontopop.api.Ontology;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class ApiTest {

    private Ontology ontology;
    private Scanner scanner = new Scanner(System.in);

    public ApiTest() {
        ontology = new Ontology();
    }

    public void start() {
        int action;
        do {
            action = getAction();

            switch (action) {
                case 1: loadOntologyFile(); break;
                case 2: listAllFood(); break;
                case 3: listAllConditions(); break;
                case 4: getFoodNutrient(); break;
                case 5: getConditionSymptom(); break;
                case 6: getConditionNeededNutrients(); break;
                case 7: getConditionRecommendedFood(); break;
            }

        } while (action != 8);

        System.out.println("Bye");
    }

    private void listAllFood() {
        List<Food> foodList = ontology.getAllFood();

        for (int i = 0; i < foodList.size(); i++) {
            System.out.println(foodList.get(i).getName());
        }
    }

    private void listAllConditions() {
        List<Condition> conditionList = ontology.getAllConditions();

        for (int i = 0; i < conditionList.size(); i++) {
            System.out.println(conditionList.get(i).getName());
        }
    }

    private void getFoodNutrient() {
        System.out.println("Enter nutrient name");
        String nutrient = scanner.next();

        System.out.println("Enter nutrient amount");
        int value = scanner.nextInt();
        List<Food> foodList = ontology.getAllFood(nutrient, value);
        for (int i = 0; i < foodList.size(); i++) {
            System.out.println(foodList.get(i).getName());
        }
    }

    private void loadOntologyFile() {
        try {
            System.out.println("Enter filename to load");
            String filename = scanner.next();
            ontology.loadOntology(filename);
        } catch (FileNotFoundException e) {

        }

        System.out.println("Ontology loaded");
    }

    private void getConditionSymptom() {
        System.out.println("Enter condition name");
        String conditionName = scanner.next();

        Condition condition = ontology.getCondition(conditionName);
        List<String> symptoms = condition.getSymptoms();

        for (int i = 0; i < symptoms.size(); i++) {
            System.out.println(symptoms.get(i));
        }
    }

    private void getConditionNeededNutrients() {
        System.out.println("Enter condition name");
        String conditionName = scanner.next();

        Condition condition = ontology.getCondition(conditionName);
        List<String> nutrients = condition.getNutrientDeficiencies();

        for (int i = 0; i < nutrients.size(); i++) {
            System.out.println(nutrients.get(i));
        }
    }

    private void getConditionRecommendedFood() {
        System.out.println("Enter condition name");
        String conditionName = scanner.next();

        Condition condition = ontology.getCondition(conditionName);
        List<Food> foods = condition.getRecommendedFoodItems();

        for (int i = 0; i < foods.size(); i++) {
            System.out.println(foods.get(i));
        }
    }

    private int getAction() {
        System.out.println("What do you want to do?");
        System.out.println("[1] load an ontology file");
        System.out.println("[2] list all food items");
        System.out.println("[3] list all conditions");

        System.out.println("[4] get food items with based on nutrient content");
        System.out.println("[5] get symptoms of a condition");
        System.out.println("[6] get needed nutrients list of a condition");
        System.out.println("[7] get recommended food list of a condition");
        System.out.println("[8] exit");


        int choice = scanner.nextInt();

        if (choice < 1 || choice > 8) {
            System.out.println("Invalid choice.");
            choice = getAction();
        }

        return choice;
    }

}
